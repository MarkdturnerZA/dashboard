import Cookies from 'js-cookie';

export const fakeAuth = {
    isAuthenticated: Boolean(Cookies.get('jwt_token')),
    userId: '',
    authenticate(callback: any, id: string) {
        const setAuth = new Promise((resolve, reject) => {
            Cookies.set('jwt_token', 'true', { expires: 7 });
            localStorage.setItem('userId', id)
            resolve(true)
        });

        setAuth.then((res) => {
            fakeAuth.isAuthenticated = Boolean(res);
            fakeAuth.userId = localStorage.getItem('userId') || '';
            callback();
        })
    },
    signout(callback: any) {
        const deleteAuth = new Promise((resolve, reject) => {
            Cookies.remove('jwt_token');
            resolve(false)
        });

        deleteAuth.then((res) => {
            fakeAuth.isAuthenticated = Boolean(res);
            localStorage.removeItem('userDetails');
            localStorage.removeItem('userId');
            callback();
        })
    }
};