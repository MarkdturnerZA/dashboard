import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Switch,
    Route,
    withRouter,
    RouteComponentProps
} from "react-router-dom";
import { Layout } from 'antd';
import Dashboard from './indexDashboard';
import Reports from './reports';
import Notifications from '../components/notifications';
import Sidebar from '../components/sidebar';
import { fakeAuth } from '../utils/login';

type MasterState = {
    isloggedOut: boolean
}

const routes = [
    {
        name: 'Dashboard',
        path: "/dashboard",
        exact: true,
        component: Dashboard
    },
    {
        name: 'Reports',
        path: "/reports",
        component: Reports,
    }
];

const { Header, Sider, Content } = Layout;

class Master extends Component<RouteComponentProps, MasterState> {
    
    state:Readonly<MasterState> = {
        isloggedOut: false
    }

    signOut = () => {
        this.setState({
            isloggedOut: true
        });
    };

    render() {
        
        if ( !fakeAuth.isAuthenticated ) {
            return <Redirect to="/login" />;
        }

        return (
            <Layout>
                <Router>
                    <Sider       
                        style={{
                            overflow: 'auto',
                            height: '100vh',
                            position: 'relative',
                            left: 0,
                        }}
                    >
                        <Sidebar signOut={this.signOut}/>
                    </Sider>
                    <Layout className="site-layout">
                        <Header className="site-layout-background" style={{ padding: 0 }}>
                            <Notifications/>
                        </Header>
                        <Content className="site-layout-background">
                                <Switch>
                                    {routes.map((route, index) => (
                                        <Route
                                            key={index}
                                            exact={route.exact}
                                            path={route.path}
                                            children={(props) => <route.component {...props} pageName={route.name} />}
                                        />
                                    ))}
                                </Switch>
                        </Content>

                    </Layout>
                </Router>
            </Layout>
        );
    }
}

export default withRouter(Master);
