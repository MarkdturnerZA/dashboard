import React from 'react';
import { PageHeader,  Descriptions } from 'antd';

interface IPageProps {
    pageName: string
}

const Dashboard = (props: IPageProps) => {
    const { pageName } = props
    return (
        <div>
            <PageHeader
                className="site-page-header"
                title={pageName}
                subTitle="This is the default page."
            />
            <div style={{ padding: '30px', background: '#fff', margin: '0 15px'}}>
                <Descriptions
                    bordered
                    title="Custom Size"
                    size="default"
                >
                    <Descriptions.Item label="Product">Cloud Database</Descriptions.Item>
                    <Descriptions.Item label="Billing">Prepaid</Descriptions.Item>
                    <Descriptions.Item label="time">18:00:00</Descriptions.Item>
                    <Descriptions.Item label="Amount">$80.00</Descriptions.Item>
                    <Descriptions.Item label="Discount">$20.00</Descriptions.Item>
                    <Descriptions.Item label="Official">$60.00</Descriptions.Item>
                    <Descriptions.Item label="Config Info">
                        Data disk type: MongoDB
                        <br />
                        Database version: 3.4
                        <br />
                        Package: dds.mongo.mid
                        <br />
                        Storage space: 10 GB
                        <br />
                        Replication factor: 3
                        <br />
                        Region: East China 1<br />
                    </Descriptions.Item>
                </Descriptions>
                <Descriptions
                    title="Custom Size"
                    size="default"
                >
                    <Descriptions.Item label="Product">Cloud Database</Descriptions.Item>
                    <Descriptions.Item label="Billing">Prepaid</Descriptions.Item>
                    <Descriptions.Item label="time">18:00:00</Descriptions.Item>
                    <Descriptions.Item label="Amount">$80.00</Descriptions.Item>
                    <Descriptions.Item label="Discount">$20.00</Descriptions.Item>
                    <Descriptions.Item label="Official">$60.00</Descriptions.Item>
                </Descriptions>
            </div>
        </div>
    )
}

export default Dashboard;