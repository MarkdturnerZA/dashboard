import React, { Component } from 'react';
import styled from 'styled-components';
import {  Form, Select } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { withRouter, Redirect, RouteComponentProps } from "react-router-dom";
import { fakeAuth } from '../utils/login';
import Logo from '../img/logo.png'

type MasterState = {
    isloggedOut: boolean,
}

const { Option } = Select;

const Wrapper = styled.section`
    display: flex;
    background: #03152a;
    height: 100vh;
    justify-content: center;
    align-items: center;
    position: fixed;
    left: 0;
    width: 100%;
    z-index: 1000;
    top: 0;
`
const FormContainer = styled.div`
    background: #dadada;
    width: 30vw;
    display: flex;
    justify-content: center;
    flex-direction: column;
    padding: 50px;
    box-shadow: 0px 0px 10px 0px rgb(8 8 8 / 59%);
    border-radius: 5px;
    
    .ant-select-single.ant-select-lg:not(.ant-select-customize-input) .ant-select-selector,
    .ant-select-single.ant-select-lg:not(.ant-select-customize-input):not(.ant-select-customize-input) .ant-select-selection-search-input {
        height: 45px;
    }
    .ant-select-single.ant-select-lg:not(.ant-select-customize-input) .ant-select-selector .ant-select-selection-item,
    .ant-select-single.ant-select-lg:not(.ant-select-customize-input) .ant-select-selector .ant-select-selection-placeholder {
        line-height: 45px;
    }

`
const LogoContainer = styled.div`
    display: block;
    max-width: 220px;
    margin: 0 auto 30px;
    img {
        width: 100%;
    }
`

const Button = styled.button`
    width: 35%;
    height: 45px;
    font-size: 16px;
    outline: none;
    border: none;
    color: #fff;
    background: #45a5cc;
    &:hover {
        cursor: pointer;
    }
`

const CTAContainer = styled.div`
    margin-top: 40px;
    .ant-form-item-control-input-content {
        justify-content: space-between;
        display: flex;
    }
`

class Login extends Component<RouteComponentProps, MasterState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            isloggedOut: true,
        };
    }
    
    private formRef = React.createRef<FormInstance>();

    logIn = ({ user }: any) => {
        const url = { from: { pathname: "/" } }
        fakeAuth.authenticate(() => {
            this.setState({
                isloggedOut: false,
            });
            this.props.history.replace(url.from);
        }, user);
    };

    onReset = () => {
        this.formRef?.current?.resetFields();
    };

    render() {
        const { isloggedOut } = this.state;

        if ( !isloggedOut ) {
            return <Redirect to="/" />;
        }
        return (
            <Wrapper>
                <FormContainer>
                    <LogoContainer>
                        <img src={Logo} alt="Logo" />
                    </LogoContainer>
                    <Form ref={this.formRef} name="control-ref" onFinish={(value) => this.logIn(value)}>
                        <Form.Item name="user" rules={[{ required: true }]}>
                            <Select
                                placeholder="Select user"
                                allowClear
                                size="large"
                            >
                                <Option value="1">User 1</Option>
                                <Option value="2">User 2</Option>
                                <Option value="3">User 3</Option>
                                <Option value="4">User 4</Option>
                            </Select>
                        </Form.Item>
                        <CTAContainer>

                            <Form.Item>
                                <Button type="submit">
                                    Submit
                                </Button>
                                <Button type="button" onClick={this.onReset}>
                                    Reset
                                </Button>
                            </Form.Item>
                        </CTAContainer>
                    </Form>
                </FormContainer>
            </Wrapper>
        );
    }
}

export default withRouter(Login);