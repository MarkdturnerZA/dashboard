import React from 'react';
import { PageHeader, Timeline } from 'antd';

interface IPageProps {
    pageName: string
}

const Reports = (props: IPageProps) => {
    const { pageName } = props
    return (
        <div>
            <PageHeader
                className="site-page-header"
                title={pageName}
                subTitle="Retrieve all reports"
            />
             <div style={{ padding: '30px', background: '#fff', margin: '0 15px'}}>
                <Timeline>
                    <Timeline.Item>Create a services site 2015-09-01</Timeline.Item>
                    <Timeline.Item>Solve initial network problems 2015-09-01</Timeline.Item>
                    <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                    <Timeline.Item>Network problems being solved 2015-09-01</Timeline.Item>
                </Timeline>
            </div>
        </div>
    )
}

export default Reports;