import { useState, useEffect, useReducer } from 'react';
import axios from 'axios';

interface DataType {
	id: number;
	name: string;
	username: string;
	email: string;
}

interface InitialStateType {
	status: string;
	isLoading: true;
	isError: boolean;
	data: DataType[];
}

type ActionType = {
	type: 'FETCH_INIT' | 'FETCH_SUCCESS' | 'FETCH_FAILURE',
	payload: any
}

const initialState: InitialStateType = {
	status: 'FETCH_INIT',
	isLoading: true,
	isError: false,
	data: [],
};

const dataFetchReducer = (state: any, action: ActionType) => {
	switch (action.type) {
		case 'FETCH_INIT':
			return {
				...state,
				isLoading: true,
				isError: false,
			};
		case 'FETCH_SUCCESS':
			return {
				...state,
				isLoading: false,
				isError: false,
				data: action.payload,
			};
		case 'FETCH_FAILURE':
			return {
				...state,
				isLoading: false,
				isError: true,
			};
		default:
			throw new Error();
	}
};

const useFetch = (query: string) => {
	const [url, setUrl] = useState(process.env.REACT_APP_ENDPOINT + query);
	const [state, dispatch] = useReducer(dataFetchReducer, initialState);
	const cache = localStorage.getItem('userDetails');
	 
	useEffect(() => {
		let didCancel = false;
	 
		const fetchData = async () => {
			dispatch({ type: 'FETCH_INIT', payload: [] });
			if (cache) {
				const data = JSON.parse(cache);
				dispatch({ type: 'FETCH_SUCCESS', payload: data });
			} else {

				try {
					const result = await axios(url);
					if (!didCancel) {
						localStorage.setItem('userDetails', JSON.stringify(result.data));
						dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
					}
				} catch (error) {
					if (!didCancel) {
						dispatch({ type: 'FETCH_FAILURE', payload: [] });
					}
				}
			}
		};
	 
		fetchData();
	 
		return () => {
		  didCancel = true;
		};
	  }, [url, cache]);
	 
	  return [state, setUrl];
};

export default useFetch