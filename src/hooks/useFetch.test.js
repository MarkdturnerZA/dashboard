import React from "react";
import { mount } from "enzyme";
import toJSON from "enzyme-to-json";
import { act } from "react-dom/test-utils";
import Profile from '../components/profile';

jest.mock("./useFetch", () => {
	return {
		__esModule: true,
		default: async () => [
			{
				"id": 1,
				"name": "Leanne Graham",
				"username": "Bret",
				"email": "Sincere@april.biz",
			}
		]
	};
});

test("matches snapshot", async () => {
	let wrapper;
	
	await act(async () => {
		wrapper = mount(
			<Profile 
				name="Leanne Graham"
				username="Bret"
			/>
		);
	});

	wrapper.update();
	
	expect(toJSON(wrapper)).toMatchSnapshot();
});