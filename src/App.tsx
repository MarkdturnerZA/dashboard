import React, { FunctionComponent } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";

import { Login } from './containers';
import ProtectedRoute from './components/protectedRoute';
import Dashboard from './containers/dashboard';
import './App.css';

const App:FunctionComponent = () => {
    return (
        <Router>
            <Switch>
              <Route path="/login">
                <Login />
              </Route>
              <ProtectedRoute path="/dashboard">
                <Dashboard />
              </ProtectedRoute>
              <Route exact path="/">
                <Redirect exact from="/" to="dashboard" />
              </Route>
              <Route path="*">
                <Redirect from="/" to="dashboard" />
              </Route>
            </Switch>
        </Router>
    );
}

export default App;
