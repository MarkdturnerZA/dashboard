import React from 'react';
import styled from 'styled-components';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';

interface IProfile {
    name: string;
    username: string;
}

const StyledContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 5vh 0 2.5vh;
`

const StyledProfileName = styled.h2`
    color: #fff;
    display: block;
    line-height: 1;
    margin: 16px 0 8px;
`
const StyledUsername = styled.p`
    color: #ccc;
    display: block;
    line-height: 1;
    font-size: 16px;
`

const Profile = (props: IProfile) => {
    const { name, username } = props;
    return (
        <StyledContainer>
            <Avatar size={120} icon={<UserOutlined />} />
            <StyledProfileName>{ name }</StyledProfileName>
            <StyledUsername>{ username }</StyledUsername>
        </StyledContainer>
    );
}

export default Profile;