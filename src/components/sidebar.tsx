import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import get from 'lodash/get';
import {
    HomeOutlined,
    ReconciliationOutlined,
    UserSwitchOutlined,
    LoadingOutlined
} from '@ant-design/icons';
import { useHistory } from "react-router-dom";
import { fakeAuth } from '../utils/login';
import Logo from '../img/logo.png'
import useFetch from '../hooks/useFetch';
import Profile from './profile'

const LogoContainer = styled.div`
    display: block;
    width: 100%;
    padding: 10px 20px 0;
    img {
        width: 100%;
    }
`

const Button = styled.button`
    background: transparent;
    border: none;
    margin: 0;
    padding: 0;
    &:hover {
        cursor: pointer;
    }
    &:focus {
        outline: none;
    }
`

const LoadingContainer = styled.div`
    min-height: 250px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    span {
        font-size: 40px;
    }
    p {
        margin: 0;
        display: block;
        line-height: 3;
    }
`

const signOut = (history: any, signOut: any ) => {
    fakeAuth.signout(() => {
        signOut();
        history.push("/login");
    });
}

const Sidebar = ( props: any ) => {
    const history = useHistory();
    const userId = localStorage.getItem('userId');
    const response = useFetch(`users/${userId}`);
    const { data, isLoading, isError } = response[0];
    const name = get(data, 'name', '');
    const username = get(data, 'username', '');

    if ( isError ) {
        return (
            <div>
                Oops!!! Semething went wrong :(
            </div>
        )
    }

    return (
        <>
            <LogoContainer>
                <img src={Logo} alt="Logo" />
            </LogoContainer>
           
            { isLoading ?
                <LoadingContainer>
                    <LoadingOutlined />
                    <p>Loading Profile</p>
                </LoadingContainer> :
                <Profile
                    name={name}
                    username={username}
                />
            } 
            <Menu
                mode="inline"
                theme="dark"
                style={{ width: '100%' }}
            >
                <Menu.Item key="1" icon={<HomeOutlined />}>
                    <Link to="/dashboard">Dashboard</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<ReconciliationOutlined />}>
                    <Link to="/reports">Reports</Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<UserSwitchOutlined />}>
                    <Button type="button" onClick={() => signOut(history, props.signOut)}>Logout</Button>
                </Menu.Item>
            </Menu>

        </>
    );
}

export default Sidebar;