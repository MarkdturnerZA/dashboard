import React from "react";
import { Route, Redirect } from "react-router-dom";
import { fakeAuth } from '../utils/login';

const ProtectedRoute = ({ component: Component, ...rest }: any) => {
  return (
        <Route
            { ...rest }
            render={ props =>
                fakeAuth.isAuthenticated === true ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
};

export default ProtectedRoute;
