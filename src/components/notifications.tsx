import React from 'react';
import styled from 'styled-components';
import { Avatar } from 'antd';
import { UserOutlined, MessageOutlined, CloudSyncOutlined, BellOutlined } from '@ant-design/icons';

const StyledContainer = styled.div`
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    justify-content: flex-end;
    margin-right: 20px;
`

const StyledIconsContainer = styled.div`
    margin-right: 15px;
    line-height: 1;
    span {
        color: #fff;
        font-size: 18px;
        margin: 0px 6px;

    }
`

/**
 * @returns {JSX}
 */
const Notifications = () => {
    return (
        <StyledContainer>
            <StyledIconsContainer>
                <BellOutlined />
                <CloudSyncOutlined />
                <MessageOutlined />
            </StyledIconsContainer>
            <Avatar size={30} icon={<UserOutlined />} />
        </StyledContainer>
    );
}

export default Notifications;